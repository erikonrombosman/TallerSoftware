<?php session_start(); 
require_once "..\controller\controller.php";
require_once "..\controller\controladorClaveCoordenadas.php";
?>
!DOCTYPE html>
<html lang="en">

<!--HEAD -->
 <?php include "head.php" ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<?php include "navbar.php" ?>
 <!-- Navigation-->
  <div class="content-wrapper">
    <div class="card card-reg-ind mx-auto mt-10">
      <div class="card-header">Realizar Transferencia</div>
        <div class="card-body">
            <table class="table table" id="dataTable" width="100%" cellspacing="0">
              <thead >
                <tr>
                  <th>Número Cuenta</th>
                  <th>Saldo</th>
                  <th>Tipo Cuenta</th>
                </tr>
              </thead>
              <tbody>
                
                <?php
                $mvc = new MvcController();
                 $mvc->cuentasUsuarioController($_SESSION["rut"]); ?>
              </tbody> 
            </table>

            <br> <br>
          <form method="post">

              <div class="col-md-12 form-row">
               <div class= col-md-3>Número de Cuenta Origen:</div>
                <input class="col-md-8" name="numCuentaOrigen" type="text" aria-describedby="emailHelp" placeholder="Ingrese número de cuenta que desea usar">
              </div>
              </br>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Número de Cuenta Destino:</div>
                <input class="col-md-8" name="numCuentaDestino" type="text" aria-describedby="emailHelp" placeholder="Ingrese número de cuenta a la que desea transferir">
              </div>
              </br>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Monto:</div>
                <input class="col-md-8" name="monto" type="text" aria-describedby="emailHelp" placeholder="Ingrese monto a transferir">
              </div>
              </br>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Mensaje:</div>
                <input class="col-md-8" name="mensaje" type="text" aria-describedby="emailHelp" placeholder="Mensaje de la transferencia">
                
              </div>
              <br><br>
              <?php
                  
                  $mc = new controladorClaveCoordenadas();
                  
                  $arrayCoordenadas = $mc->crearCoordenadas();
                  $c1 = $arrayCoordenadas[0];
                  $c2 = $arrayCoordenadas[1];
                  $c3 = $arrayCoordenadas[2];
                  echo '<div class="col-md-12 form-row">
                      <div class= col-md-2>'.$c1.'</div> &nbsp&nbsp 
                      <div class= col-md-2>'.$c2.'</div> &nbsp&nbsp
                      <div class= col-md-2>'.$c3.'</div>
                      </div> <br>'; ?>
                  <div class="col-md-12 form-row">
                      <input name="box1" type="text" class= col-md-2> &nbsp&nbsp 
                      <input name="box2" type="text" class= col-md-2> &nbsp&nbsp
                      <input name="box3" type="text" class= col-md-2>&nbsp&nbsp &nbsp&nbsp &nbsp&nbsp 
                      <input type="submit" value="Transferir" class="btn btn-primary btn-block col-md-4">
                      <input type="hidden" name="C1" value="<?php echo $c1;?>" >
                      <input type="hidden" name= "C2" value="<?php echo $c2;?>" >
                      <input type="hidden" name= "C3" value="<?php echo $c3;?>" >
                  </div> 

            
          </form>

          <?php
          $mvc->realizarTransferencia($mc);
          ?>
        </div>
      </div>
      <!-- Example DataTables Card-->

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Your Website 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>

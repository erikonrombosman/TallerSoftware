<?php session_start(); 
require_once "..\controller\controller.php"
?>
!DOCTYPE html>
<html lang="en">

<!--HEAD -->
 <?php include "head.php" ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<?php include "navbarCajero.php" ?>
 <!-- Navigation-->
  <div class="content-wrapper">
    <div class="card card-reg-ind mx-auto mt-10">
      <div class="card-header">Realizar Depósito</div>
        <div class="card-body">
          <form>
              <div class="col-md-12 form-row">
               <div class= col-md-3>Monto a depositar:</div>
                <input class="col-md-8"  type="text" name="monto" placeholder="Ingrese monto a transferir">
              </div>
              </br>
               <div class="col-md-12 form-row">
               <div class= col-md-3>Cuenta Destino:</div>
                <input class="col-md-8"  type="text" name="cuentaDes" placeholder="Ingrese Cuenta Destino">
              </div>
              <br><br>
              <div class="col-md-12 form-row"> 

                <input type="submit" value="Validar Depósito" class="btn btn-primary btn-block col-md-4">
               

            </div>
          </form>
        </div>
      </div>
      <!-- Example DataTables Card-->

    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © ChuckNorris Inc. 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Cerrar Sesión ATM</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="login.html">Cerrar Sesión</a>
          </div>
        </div>
      </div>
    </div>
    
    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>

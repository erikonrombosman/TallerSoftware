<!DOCTYPE html>
<html lang="es">
<?php session_start(); 
require_once "..\controller\controller.php"
?>

<!--HEAD -->
 <?php include "head.php" ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">



  <!-- Navigation-->
 <?php include "navbar.php" ?>




  <!-- Contenido -->
  <div class="content-wrapper">
    <div class="container-fluid">

      <!-- Breadcrumbs-->


      <!-- DataTable: Paciente-->
      <div class="card mb-3">
        <div class="card-header">
          <i class="fa fa-table"></i> Historial</div>
        <div class="card-body">
            <table class="table table" id="dataTable" width="100%" cellspacing="0">
              <thead >
                <tr>
                  <th>Número Cuenta</th>
                  <th>Saldo </th>
                  <th>Tipo Cuenta</th>
                </tr>
              </thead>
              <tbody>
                
                <?php
                $mvc = new MvcController();
                 $mvc->cuentasUsuarioController($_SESSION["rut"]); ?>
              </tbody> 
            </table> <br><br>
            <form method="POST">
               <div class="col-md-12 form-row">
                      <div class= col-md-2>Número de Cuenta: </div>
                      <input class="form-control col-md-6"  type="text" placeholder="Número De Cuenta" name="selectCuenta">
                      &nbsp&nbsp&nbsp&nbsp
                      <input type="submit" value="Enviar" class="btn btn-primary btn-block col-md-3">
              </div>
            </form><br><br>
                        <table class="table table" id="dataTable2" width="100%" cellspacing="0">
              <thead >
                <tr>
                  <th>Código Transacción</th>
                  <th>Fecha Transacción </th>
                  <th>Monto Transacción</th>
                  <th>Tipo Transacción</th>
                  <th>Descripción</th>
                  <th>Cuenta Emisor</th>
                  <th>Cuenta Receptor</th>
                </tr>
              </thead>
              <tbody>
                
                <?php
                 $mvc->historialCuentaController($_SESSION["rut"]); 

                 ?>
              </tbody> 
            </table>
           
        </div>
        <div class="card-footer small text-muted"></div>
      </div>
    </div>
    <!-- /.container-fluid-->
    <!-- /.content-wrapper-->
    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © ICCI UCN 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->
    <div class="modal fade" id="modalLogOut" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
      <div class="modal-dialog" role="document">
        <div class="modal-content">
          <div class="modal-header">
            <h5 class="modal-title" id="exampleModalLabel">Listo para irte?</h5>
            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">×</span>
            </button>
          </div>
          <div class="modal-body">Presiona "Logout" si estás listo para cerrar sesión.</div>
          <div class="modal-footer">
            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancelar</button>
            <a class="btn btn-primary" href="login.html">Logout</a>
          </div>
        </div>
      </div>
    </div>

    <!-- MODALS -->
    <!-- Modal: Eliminar Paciente -->


    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Inicializa algunos datos de ejemplo -->
    
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
  </div>
</body>

</html>

<!DOCTYPE html>
<html lang="en">

<!--HEAD -->
<?php include "head.php";
session_start(); ?>

<body class="fixed-nav sticky-footer bg-dark" id="page-top">
  <!-- Navigation-->
<?php include "navbarATM.php" ?>
  <!--FIN DE LAS BARRAS DE NAVEGACIÓN!!!! -->

<div class="content-wrapper">
    <div class="card card-reg-ind mx-auto mt-10">
      <div class="card-header">Datos de la Cuenta</div>
      <div class="card-body">
        <form>
          <?php 
          require_once "..\controller\controller.php";
          $mvc = new MvcController();
          $datos = $mvc->datosCuentaATMController($_SESSION["tarjeta"]);
          ?> 
          <div class="form-group">
               <div class="col-md-12 form-row">
                  <div class="col-md-12 form-row">
                      <div class= col-md-3>Rut:</div>
                      <div class="col-md-9" id="rut"> <?php  echo $_SESSION["tarjeta"]; ?> </div>
                  </div> <br><br>
                  <div class= col-md-3>Numero Cuenta:</div>
                   <div class="col-md-9" id="direccion" > <?php echo utf8_encode($datos["numCuenta"]); ?> </div> <br><br>
                  <div class= col-md-3>Saldo:</div>
                   <div class="col-md-9" id="direccion" > <?php echo utf8_encode($datos["saldo"]); ?> </div> <br><br>
                   <div class= col-md-3>Tipo de Cuenta:</div>
                   <div class="col-md-9" id="direccion" > <?php echo utf8_encode($datos["descrpTipoCuenta"]); ?> </div> </br><br><br>
        <!-- COMENTÉ ESTO PORQUE NO LO NECESITO EN ESTA VISTA
        <div class="card-body">
          <div class="table-responsive">
            <table class="table table" id="dataTable" width="100%" cellspacing="0">
              <thead >
                <tr>
                  <th>Número Cuenta</th>
                  <th>Tipo de Cuenta </th>
                  <th>Saldo</th>
                </tr>
              </thead>
              <tbody>
                <?php $mvc->cuentasUsuarioController($_SESSION["rut"]); ?>
              </tbody>
            </table>
          </div>
        </div>
        -->              

          </div>
          </br> </br>
 

      </div>
    </div> 
    </div>


    <footer class="sticky-footer">
      <div class="container">
        <div class="text-center">
          <small>Copyright © Stevan Co. 2017</small>
        </div>
      </div>
    </footer>
    <!-- Scroll to Top Button-->
    <a class="scroll-to-top rounded" href="#page-top">
      <i class="fa fa-angle-up"></i>
    </a>
    <!-- Logout Modal-->

    <!-- Bootstrap core JavaScript-->
    <script src="vendor/jquery/jquery.min.js"></script>
    <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
    <!-- Core plugin JavaScript-->
    <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
    <!-- Page level plugin JavaScript-->
    <script src="vendor/chart.js/Chart.min.js"></script>
    <script src="vendor/datatables/jquery.dataTables.js"></script>
    <script src="vendor/datatables/dataTables.bootstrap4.js"></script>
    <!-- Custom scripts for all pages-->
    <script src="js/sb-admin.min.js"></script>
    <!-- Custom scripts for this page-->
    <script src="js/sb-admin-datatables.min.js"></script>
    <script src="js/sb-admin-charts.min.js"></script>
  </div>
</body>

</html>

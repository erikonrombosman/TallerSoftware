<?php

#EXTENSIÓN DE CLASES: Los objetos pueden ser extendidos, y pueden heredar propiedades y métodos. Para definir una clase como extensión, debo definir una clase padre, y se utiliza dentro de una clase hija.

require_once "conexion.php";

class Crud extends Conexion{

	#INGRESO USUARIO
	#-------------------------------------
	public static function ingresoUsuarioModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT rutCliente, password FROM cliente WHERE rutCliente = :usuario");	
		$stmt->bindParam(":usuario", $datosModel["rut"], PDO::PARAM_STR);
		$stmt->execute();
		return $stmt->fetch();
		$stmt->close();
	}

	public static function ingresoTarjetaModel($datosModel, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT codTarjeta, password FROM Tarjeta WHERE codTarjeta = :usuario");	
		$stmt->bindParam(":usuario", $datosModel["tarjeta"], PDO::PARAM_STR);
		$stmt->execute();
		return $stmt->fetch();
		$stmt->close();
	}
	public static function ingresoCajeroModel($datosModel, $tabla){

		$stmt = Conexion::conectar()->prepare("SELECT rutCajero, passwordCajero FROM usuariocajero WHERE rutCajero = :rutcajero");	
		$stmt->bindParam(":rutcajero", $datosModel["Cajero"], PDO::PARAM_STR);
		$stmt->execute();

		return $stmt->fetch();
		$stmt->close();
	}

	public static function datosUserModel($rutCliente, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT nombre, direccion, telefono from $tabla where rutCliente = :rutCliente");
		$stmt->bindParam(":rutCliente", $rutCliente, PDO::PARAM_STR);
		$stmt->execute();
		return $stmt->fetch();
		$stmt->close();
	}


	public static function datosCuentaATMModel($codTarjeta, $tabla){
		$stmt = Conexion::conectar()->prepare("SELECT c.numCuenta numCuenta, c.saldo saldo, tc.descripcion descrpTipoCuenta
												from cuentabancaria c, tipocuenta tc, tarjeta t
												where t.codTarjeta = :codTarjeta
												and t.codCuentaBancaria = c.numCuenta
												and c.codTipoCuenta = tc.codTipoCuenta;");
		$stmt->bindParam(":codTarjeta", $codTarjeta, PDO::PARAM_STR);
		$stmt->execute();
		return $stmt->fetch();
		$stmt->close();
	}

	 public static function cuentasUsuarioModel($rutCliente){
		$stmt = Conexion::conectar()->prepare("SELECT cb.numcuenta ncuent, cb.saldo sald, tipoc.descripcion descr from tipocuenta tipoc, cliente cli, cuentabancaria cb, cuentabancaria_cliente cb_c where cli.rutcliente = :rutCliente and tipoc.codtipocuenta = cb.codtipocuenta and cb.numcuenta = cb_c.numcuenta and cb_c.rutcliente = cli.rutcliente;");
		$stmt->bindParam(":rutCliente", $rutCliente, PDO::PARAM_STR);
		/*$stmt = Conexion::conectar()->prepare("SELECT rutCliente, nombre, direccion from cliente");*/
		$stmt->execute();

		return $stmt->fetchAll();
		$stmt->close();

	}
	public static function cuentasModel($numCuenta){
		$stmt = Conexion::conectar()->prepare("SELECT numcuenta numcuenta, saldo saldofrom cuentabancaria where numCuenta = :numCuenta;");
		$stmt->bindParam(":numcuenta", $numCuenta, PDO::PARAM_STR);
		/*$stmt = Conexion::conectar()->prepare("SELECT rutCliente, nombre, direccion from cliente");*/
		$stmt->execute();

		return $stmt->fetchAll();
		$stmt->close();

	}

	public static function cuentasModelConRut($numCuenta, $rut){
		$stmt = Conexion::conectar()->prepare("SELECT cb.numcuenta numcuenta, cb.saldo saldo, cc.rutCliente rut from cuentabancaria cb, CuentaBancaria_Cliente cc where cb.numCuenta = :numCuenta and cb.rutCliente = :rut;");
		$stmt->bindParam(":numcuenta", $numCuenta, PDO::PARAM_STR);
		$stmt->bindParam(":rut", $rut, PDO::PARAM_STR);
		/*$stmt = Conexion::conectar()->prepare("SELECT rutCliente, nombre, direccion from cliente");*/
		$stmt->execute();

		return $stmt->fetchAll();
		$stmt->close();

	}

	public static function historialCuentaModel($numCuenta){
		$stmt = Conexion::conectar()->prepare("SELECT t.codtrans, t.fechatrans, t.montotrans,
												tipot.nomtipotrans, t.descripcionopcional,
												t.numcuentaemisor, t.numCuentReceptor
												from transaccion t, tipotransaccion tipot
												where ( t.numcuentaemisor = :numCuenta or t.numCuentReceptor = :numCuenta2 )
												and t.codtipotrans = tipot.codtipotrans
												order by t.fechatrans;");

		$stmt->bindParam(":numCuenta", $numCuenta, PDO::PARAM_INT);
		$stmt->bindParam(":numCuenta2", $numCuenta, PDO::PARAM_INT);

		$stmt->execute();

		return $stmt->fetchAll();
		$stmt->close();

	}

	public static function montoCuentaTarjeta($codTarjeta){ //, cd.numcuenta numcuenta
		$stmt = Conexion::conectar()->prepare("SELECT cb.saldo saldo from cuentabancaria cb, tarjeta t where cb.numcuenta = t.codcuentabancaria and t.codtarjeta = :codigoTarj;");
		$stmt->bindParam(":codigoTarj", $codTarjeta, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row[0]; 
		$stmt->close();
	}
	public static function montoCuentaCliente($rutCliente){ //, cd.numcuenta numcuenta
		$stmt = Conexion::conectar()->prepare("SELECT cb.saldo saldo from cuentabancaria cb, cuentabancaria_cliente cbcli where cb.numCuenta = cbcli.numCuenta and cbcli.rutCliente = :rut ;");
		$stmt->bindParam(":rut", $rutCliente, PDO::PARAM_STR);
		$stmt->execute();
		$row = $stmt->fetch();
		return $row[0]; 
		$stmt->close();
	}

		public static function actualizarSaldo($numCuenta, $monto){ //monto puede ser negativo o positivo
			$stmt = Conexion::conectar()->prepare("UPDATE cuentabancaria set saldo = saldo + :monto where numcuenta = :numcuenta;");
			$stmt->bindParam(":monto", $monto, PDO::PARAM_STR);
			$stmt->bindParam(":numcuenta", $numCuenta, PDO::PARAM_STR);
			$stmt->execute(); 
			//$stmt->close();
		}
}


?>
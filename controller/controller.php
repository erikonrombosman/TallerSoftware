<?php

include "crud.php";

class MvcController{
	//Portal Web
	public function ingresoUsuarioController(){
		if(isset($_POST["selectRut"])){
			$datosController = array( "rut"=>$_POST["selectRut"], 
								      "password"=>$_POST["selectClave"]);
			$respuesta = Crud::ingresoUsuarioModel($datosController, "cliente");
			if($respuesta["rutCliente"] == $_POST["selectRut"] && $respuesta["password"] == $_POST["selectClave"]){
				include "model/cliente.php";
				session_start();
				$_SESSION["rut"] = $respuesta["rutCliente"];
				header("location:view/perfilUser.php");
			}
			else{
				header("location:index.php?action=fallo");
			}

		}	

	}

	//ATM
	public function ingresoTarjetaController(){
		if(isset($_POST["selectTarjeta"])){
			$datosController = array( "tarjeta"=>$_POST["selectTarjeta"], 
								      "password"=>$_POST["selectClave"]);
			$respuesta = Crud::ingresoTarjetaModel($datosController, "cliente");
			if($respuesta["codTarjeta"] == $_POST["selectTarjeta"] && $respuesta["password"] == $_POST["selectClave"]){
				//include "model/cliente.php";
				session_start();
				$_SESSION["tarjeta"] = $respuesta["codTarjeta"];
				header("location:view/perfilATM.php");
			}
			else{
				header("location:index2.php?action=fallo");
			}

		}		
	}

	//Cajero
	public function ingresoCajeroController(){
		if(isset($_POST["selectCajero"])){
			$datosController = array( "Cajero"=>$_POST["selectCajero"], 
								      "password"=>$_POST["selectClave"]);
			$respuesta = Crud::ingresoCajeroModel($datosController, "usuariocajero");
			if($respuesta["rutCajero"] == $_POST["selectCajero"] && $respuesta["passwordCajero"] == $_POST["selectClave"]){
				//include "model/cliente.php";
				session_start();
				$_SESSION["Cajero"] = $respuesta["rutcajero"];
				header("location:view/perfilCajero.php");
			}
			else{
				header("location:index3.php?action=fallo");
			}

		}		
	}

	//Cajero
	public function ingresoCajeroClienteController(){
		if(isset($_POST["selectRut"])){
			$datosController = array( "rut"=>$_POST["selectRut"]);
			$respuesta = Crud::ingresoUsuarioModel($datosController, "cliente");
			if($respuesta["rutCliente"] == $_POST["selectRut"]){
				session_start();
				$_SESSION["rutCliente"] = $respuesta["rutCliente"];
				header("location:perfilCajeroCliente.php");
			}
			else{
				header("location:perfilCajero.php?action=fallo");
			}

		}	

	}

	//Portal Web
	public function datosUserController($rut){
		$datos = Crud::datosUserModel($rut, "cliente");
		return $datos;
	}

	//ATM
	public function datosCuentaATMController($numTarjeta){
		$datos = Crud::datosCuentaATMModel($numTarjeta, "tarjeta");
		return $datos;
	}

	//Portal Web
	public function cuentasUsuarioController($rut){
		
		$respuesta = Crud::cuentasUsuarioModel($rut);
		
		foreach ($respuesta as $row => $item) {
			
			echo '<tr>
						<td>'.$item[0].'</td>
						<td>'.$item[1].'</td>
						<td>'.$item[2].'</td>
						

				 </tr>';
		}
	}

	//Portal Web
	public function historialCuentaController(){
		if(isset($_POST["selectCuenta"])){
			$numCuenta = $_POST["selectCuenta"];
			$respuesta = Crud::historialCuentaModel($numCuenta);
			if($respuesta){
				foreach ($respuesta as $row => $item) {
						echo '<tr>
								<td>'.$item[0].'</td>
								<td>'.$item[1].'</td>
								<td>'.$item[2].'</td>
								<td>'.$item[3].'</td>
								<td>'.$item[4].'</td>
								<td>'.$item[5].'</td>
								<td>'.$item[6].'</td>
						 </tr>';
					}
			}else{
				echo "<br><div class= 'alert alert-warning' role='alert'>  No tiene transacciones  </div>" ;
			}
		}
	}

	//ATM
	public function retiroATMController($codTarjeta){
		if(isset($_POST["monto"])){
			$montoR = $_POST["monto"];
			$montoActual = Crud::montoCuentaTarjeta($codTarjeta);
			if($montoActual){
				if($montoActual < $montoR){
					echo "<br><div class= 'alert alert-warning' role='alert'>El monto es inválido</div>" ;
				}else{
					//hacer retiro
					$datos = $this->datosCuentaATMController($_SESSION["tarjeta"]);
					Crud::actualizarSaldo($datos["numCuenta"], $montoR*(-1));
					echo "<br><div class= 'alert alert-success' role='alert'>Retire su dinero. </div>";
				}
			}else{
				echo "<br><div class= 'alert alert-warning' role='alert'>Cuenta inválida</div>" ;  
			}

		}
	}
	public function depositoATMController(){
		if(isset($_POST["monto"]) and isset($_POST["cuentaDes"]) and $_POST["monto"] and $_POST["cuentaDes"]){
			$montoR = $_POST["monto"];
			$cuentaActual = Crud::cuentasModel($_POST["cuentaDes"]);
			if($cuentaActual["saldo"]){
				if(0 < $montoR){
					echo "<br><div class= 'alert alert-warning' role='alert'>El monto es inválido</div>" ;
				}else{
					//hacer deposito
					Crud::actualizarSaldo($cuentaActual["numCuenta"], ($montoR+$cuentaActual["saldo"]));
					echo "<br><div class= 'alert alert-success' role='alert'>Dinero depositado. </div>";
				}
			}else{
				echo "<br><div class= 'alert alert-warning' role='alert'>Cuenta inválida</div>" ;  
			}

		}
	}


	public function insertaTransaccion($cuentaOrigen, $cuentaDestino, $monto, $descripcion, $codTipoTrans){
		//YA ESTÁ HECHA EN CONTROLLER PARA EL PORTAL WEB, SE LLAMA transferenciaPortalWebController
		$stmt = Conexion::conectar()->prepare("INSERT into transaccion values (NULL, :numCuentaEmisor, :numCuentaReceptor, sysdate(), :monto, :descripcionOpcional, :codTipoTrans);");
		
		$stmt->bindParam(":numCuentaEmisor", $cuentaOrigen, PDO::PARAM_STR);
		$stmt->bindParam(":numCuentaReceptor", $cuentaDestino, PDO::PARAM_STR);
		$stmt->bindParam(":monto", $monto, PDO::PARAM_STR);
		$stmt->bindParam(":descripcionOpcional", $descripcion, PDO::PARAM_STR);
		$stmt->bindParam(":codTipoTrans", $codTipoTrans, PDO::PARAM_STR);

		return $stmt->execute();
	}

	//Portal Web
	public function transferenciaPortalWebController($cuentaOr, $cuentaDe, $monto, $desc, $cod){

		$stmt = Conexion::conectar()->prepare("UPDATE cuentabancaria
										set saldo = saldo - :monto
										where cuentabancaria.numcuenta = :numCuentaEmisor;");
		$stmt->bindParam(":monto", $monto, PDO::PARAM_STR);
		$stmt->bindParam(":numCuentaEmisor", $cuentaOr, PDO::PARAM_STR);
		if($stmt->execute()){

			$stmt2 = Conexion::conectar()->prepare("UPDATE cuentabancaria
										set saldo = saldo + :monto
										where cuentabancaria.numcuenta = :numCuentaReceptor;");
			$stmt2->bindParam(":monto", $monto, PDO::PARAM_STR);
			$stmt2->bindParam(":numCuentaReceptor", $cuentaDe, PDO::PARAM_STR);
			if($stmt2->execute()){

				$stmt3 = $this->insertaTransaccion($cuentaOr, $cuentaDe, $monto, $desc, $cod);

				if($stmt3){

					return true;
				}else{
					return false;
				}
			}else{
				return false;
			}
		}else{
			return false;
		}
	

	}

	//¿Esto de qué es?
	public function realizarTransferencia($mcP){
		if(isset($_POST["numCuentaDestino"]) and isset($_POST["numCuentaOrigen"]) and isset($_POST["monto"]) and $_POST['monto'] and isset($_POST["box1"])and isset($_POST["box2"])and isset($_POST["box3"])){
			$box1 = $_POST["box1"];
			$box2 = $_POST["box2"];
			$box3 = $_POST["box3"];
			$cuentaOR = $_POST["numCuentaOrigen"];
			$cuentaDe = $_POST["numCuentaDestino"];
			$montoR =  $_POST["monto"];
			$c1 = $_POST["C1"];
			$c2 = $_POST["C2"];
			$c3 = $_POST["C3"];
			$desc = $_POST["mensaje"];
			  require_once "..\controller\controladorClaveCoordenadas.php";
			  $mc = $mcP;
		      $cuentasValidas = $mc->cuentasValidas();
              if($cuentasValidas){
	              $transferenciaValida = $mc->comprobarCoordenadasIngresadas($box1, $box2, $box3 ,$cuentaOR, $c1, $c2, $c3);
	              if($transferenciaValida){
	              	$boolTransfer = $this->transferenciaPortalWebController($cuentaOR, $cuentaDe, $montoR , $desc, 5);
	              	if($boolTransfer){
	                	echo '<div class="alert alert-success">Transferencia Exitosa</div>';
	                }else{
	                	echo '<div class="alert alert-danger">La transferencia no se pudo realizar</div>';
	                }
	              }else{
	                echo '<div class="alert alert-danger">Las coordenadas no son válidas</div>';
	              }
              }else{
                 echo '<div class="alert alert-danger">Las Cuentas no son válidas</div>';
              }
        }        
	}

}

?>
<?php
	class controladorClaveCoordenadas{ // todo lo que esté en esta clase poner despues en el otro controlador.
		//Al instanciarse este controlador se deben setear así las variables:
	private $arrayCoordenadas;
	private $coordenada1;
	private $coordenada2;
	private $coordenada3;

	function __construct(){
		$this->arrayCoordenadas = array("A1","A2","A3","A4","A5",
									"B1","B2","B3","B4","B5",
									"C1","C2","C3","C4","C5",
									"D1","D2","D3","D4","D5",
									"E1","E2","E3","E4","E5",
									"F1","F2","F3","F4","F5",
									"G1","G2","G3","G4","G5",
									"H1","H2","H3","H4","H5",
									"I1","I2","I3","I4","I5",
									"J1","J2","J3","J4","J5",);
		$this->coordenada1 = 0;
		$this->coordenada2 = 0;
		$this->coordenada3 = 0;
	}
	

	/**
	*	Lo que hace esta función es generar las 3 coordenadas que pedirá, que son un numero
	*	aleatorio entre 0 y 49 (que es la cantidad de strings que tiene el arrayCoordenadas).
	*	Este método debiera ejecutarse al momento de cargar la página que pide las coordenadas.
	*/
	function crearCoordenadas(){
		$this->coordenada1 = rand(0,47);
		$this->coordenada2 = $this->coordenada1;
		while($this->coordenada2 == $this->coordenada1){ //esto es para evitar que se repitan las coordenadas.
			$this->coordenada2 = rand($this->coordenada1,48);
		}
		$this->coordenada3 = $this->coordenada2;
		while($this->coordenada3 == $this->coordenada2){ //esto es para evitar que se repitan las coordenadas.
			$this->coordenada3 = rand($this->coordenada2,49);
		}

		return array($this->arrayCoordenadas[$this->coordenada1], $this->arrayCoordenadas[$this->coordenada2], $this->arrayCoordenadas[$this->coordenada3]);
	}

	function comprobarCoordenadasIngresadas($box1, $box2, $box3, $cuentaOr, $c1, $c2, $c3){ //c/u de los "box" son entradas por pantalla.
		//if(isset($_POST["box1"]) and isset($_POST["box2"]) and isset($_POST["box3"])){
			$stmt = Conexion::conectar()->prepare("SELECT matrizValores from claveunica where numCuentaBancaria = :ncuenta;");
			$stmt->bindParam(":ncuenta", $cuentaOr , PDO::PARAM_STR);
			$stmt->execute();
			$matriz = $stmt->fetch()["matrizValores"]; //VER SI FUNCIONA

			$cont1=0;
			while($this->arrayCoordenadas[$cont1] != $c1){
				$cont1 = $cont1+1;
			}
			$cont2=0;
			while($this->arrayCoordenadas[$cont2] != $c2){
				$cont2 = $cont2+1;
			}
			$cont3=0;
			while($this->arrayCoordenadas[$cont3] != $c3){
				$cont3 = $cont3+1;
			}

			$claveUnica1 = substr($matriz,$cont1*2,2);
			
			$claveUnica2 = substr($matriz,$cont2*2,2);
			
			$claveUnica3 = substr($matriz,$cont3*2,2);
			

			$coordenada1 = 0;
			$coordenada2 = 0;
			$coordenada3 = 0;

			if ($box1 == $claveUnica1 && $box2 == $claveUnica2 && $box3 == $claveUnica3){
				return true;
			}else{
				return false;
			}
		//}

	}

	function cuentasValidas(){
		if(isset($_POST["numCuentaOrigen"]) and isset($_POST["numCuentaDestino"])){
			$stmt = Conexion::conectar()->prepare("SELECT cli.rutCliente rutCli from cliente cli, cuentabancaria cu where cu.numCuenta = :ncuenta and cu.rutCliente = cli.rutCliente;");
				$stmt->bindParam(":ncuenta", $_POST["numCuentaOrigen"], PDO::PARAM_STR);
				$stmt->execute();
				$clienteRut = $stmt->fetch()[0];
				if($clienteRut == $_SESSION["rut"]){
					return True;
				}else{
					$stmt2 = Conexion::conectar()->prepare("SELECT numCuenta from cuentabancaria where numCuenta = :ncuenta;");
					$stmt2->bindParam(":ncuenta", $_POST["numCuentaOrigen"], PDO::PARAM_STR);
					$stmt2->execute();
					if($stmt2->rowCount() < 1){
						return false;
					}else{
						return true;
					}

				}
		}else{
			return False;
		}
	}

}
?>
--CUENTAS DE UNA PERSONA
select cb.numcuenta ncuent, cb.saldo sald, tipoc.descripcion descr
from tipocuenta tipoc, cliente cli, cuentabancaria cb,
cuentabancaria_cliente cb_c where cli.rutcliente = '1' --aqui en vez de '1' poner sesion.
and tipoc.codtipocuenta = cb.codtipocuenta
and cb.numcuenta = cb_c.numcuenta
and cb_c.rutcliente = cli.rutcliente;

--TRANSACCIONES PARA PORTAL WEB

--comprobando que cuentabancaria desde la que se transferirá existe Y pertenece al cliente (porque no queremos un hackerman en nuestro banco XD)
select count(*)
from cuentabancaria_cliente
where numcuenta = 111 --la que haya seleccionado el usuario
and rutcliente = "1"; --variable de sesion >:3

--comprobando que cuentabancaria a la que se desea transferir existe
select count (*)
from cuentabancaria
where cuentabancaria.numcuenta = 222; --aqui debe ser numcuenta a la que se está transfiriendo
--hacer fetch o que se sho, y luego en php:
if(existe > 0){
	//seguir con la transferencia
}else{
	//popup rojo advirtiendo que la cuenta a la que se va a transferir no existe.
}

--comprobando que saldo es suficiente para hacer transferencia
select saldo
from cuentabancaria
where numcuenta = 111; --aqui que sea la cuenta seleccionada

--actualizando saldo cuentabancaria usuario:
update cuentabancaria
set saldo = saldo - 10000 --aqui en vez de 10000 debe ser variable de saldo elegido
where cuentabancaria.numcuenta = 111; --aqui en vez de 111 debe ser el numcuenta del cliente que tiene sesion iniciada y de la cuenta que está usando.

--actualizando saldo cuentabancaria receptora de la transferencia:
update cuentabancaria
set (saldo = saldo + 10000) --aqui en vez de 10000 debe ser variable de saldo transferido.
where cuentabancaria.numcuenta = 222; --aquí en vez de 222 debe ser el numcuenta de la cuenta a la que se transferirá

--historial de transacciones de una cuenta
--recordar que las transacciones pueden ser
--abono o retiro, la unica forma de ver si es un retiro
--o abono es que, si el numero de cuenta que se está consultando
--es igual al numero de cuenta emisor, entonces es un retiro,
--sino es un abono.
select t.codtrans, t.fechatrans, t.montotrans,
	tipot.nomtipotrans, t.descripcionopcional,
	t.numcuentaemisor, t.numCuentReceptor
from transaccion t, tipotransaccion tipot
where (t.numcuentaemisor = 111 or t.numCuentReceptor = 111)
and t.codtipotrans = tipot.codtipotrans
order by t.fechatrans;
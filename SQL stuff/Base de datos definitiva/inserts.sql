use stebanco;
insert into tipoCuenta values(1, 'Cuenta de ahorros');
insert into tipoCuenta values(2, 'Cuenta Corriente Premium');
insert into tipoCuenta values(3, 'Cuenta Corriente Corporativa');

insert into TipoTransaccion values(1, 'Retiro ATM');
insert into TipoTransaccion values(2, 'Deposito ATM');
insert into TipoTransaccion values(3, 'Retiro Cajero');
insert into TipoTransaccion values(4, 'Deposito cajero');
insert into TipoTransaccion values(5, 'Transferencia web');

insert into Cliente values('1','Joe', 'Mi Casa', 666, 'hola');
insert into Cliente values('12345678-9', 'Matías Zárate','Calle Hermosa 201', 99999999,'password123');
insert into Cliente values('11111111-9', 'Sebastián Godoy','Avenida Siempre Viva 456', 88888888,'godoy!!!');
insert into Cliente values('8998765-1', 'Erik Astorga','Vivo en Cochimbo', 77777777,'microondas');
insert into Cliente values('22222222-2', 'Rosita Hormann','Vicuñalandia', 66666666,'toy resfriada :c');
insert into Cliente values('000000000000', 'Cristian Chiang','UCN', 55555555,'profesorcito');

--inserts cuentabancaria
insert into cuentabancaria values (111, 500000, '2017-03-08', null, 1);
insert into cuentabancaria values (222, 600000, '2017-04-10', null, 1);
insert into cuentabancaria values (333, 700000, '2017-05-29', null, 2);
insert into cuentabancaria values (444, 10000, '2017-02-25', null, 3);

insert into cuentabancaria_cliente values(111, '1');
insert into cuentabancaria_cliente values(222, '12345678-9');
insert into cuentabancaria_cliente values(333, '000000000000');
insert into cuentabancaria_cliente values(444, '1');
insert into cuentabancaria_cliente values(444, '22222222-2');

insert into transaccion values(9090, 111, null,'2017-11-12', 10000, 'Supermercado Merkat', 1);
insert into transaccion values(9080, 111, null,'2017-10-13', 90000, 'Deuda Internet', 3);
insert into transaccion values(9070, 111, null,'2017-09-14', 30000, 'Plata prestada', 4);
insert into transaccion values(9060, 111, null,'2017-08-15', 32000, 'Deposito efectivo', 2);
insert into transaccion values(9050, 111, null,'2017-07-16', 1000, 'Plata pa la micro', 1);
insert into transaccion values(9040, 111, null,'2017-06-17', 299990, 'Notebook', 3);
insert into transaccion values(9030, 111, null,'2017-05-18', 8000, 'Pago cuenta de la luz', 3);
insert into transaccion values(9020, 111, 222,'2017-04-19', 15000, 'Transacción a mi amigo', 3);
insert into transaccion values(7010, 222, 111,'2017-04-19', 3000, 'Hola te debía plata c:', 3);
insert into transaccion values(7020, 222, null,'2017-04-19', 13000, 'Cuenta luz', 3);
insert into transaccion values(7030, 222, null,'2017-04-19', 5000, 'Cuenta del agua', 3);
insert into transaccion values(7040, 222, null,'2017-04-19', 70000, 'Supermercado', 3);
insert into transaccion values(7050, 222, null,'2017-04-19', 25990, 'Deuda internet', 3);

insert into tarjeta values('111111111111-111111',111,1111);
insert into tarjeta values('222222222222-111111',222,2222);
insert into tarjeta values('333333333333-111111',333,3333);
insert into tarjeta values('444444444444-111111',444,4444);

insert into claveunica values(1000, 111, 'a1a2a3a4a5b1b2b3b4b5c1c2c3c4c5d1d2d3d4d5e1e2e3e4e5f1f2f3f4f5g1g2g3g4g5h1h2h3h4h5i1i2i3i4i5j1j2j3j4j5');
insert into claveunica values(2000, 222, '0102030405060708091011121314151617181920212223242526272829303132333435363738394041424344454647484950');
insert into claveunica values(3000, 333, '1111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111111');
insert into claveunica values(4000, 444, '0000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000000');

insert into usuarioCajero values("q", "asd");
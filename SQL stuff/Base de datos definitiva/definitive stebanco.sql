use stebanco;

create table TipoCuenta (
codTipoCuenta integer,
descripcion varchar(50),
primary key(codTipoCuenta));

create table TipoTransaccion(
codTipoTrans integer,
nomTipoTrans varchar(50),
primary key(codTipoTrans));

create table Cliente(
rutCliente varchar(12),
nombre varchar(50),
direccion varchar(50),
telefono integer,
password varchar(20),
primary key(rutCliente));

create table CuentaBancaria(
numCuenta integer,
saldo integer,
fechaApertura date,
fechaCierre date,
codTipoCuenta integer,
primary key(numCuenta),
foreign key(codTipoCuenta) references TipoCuenta(codTipoCuenta));

create table CuentaBancaria_Cliente(
numCuenta integer,
rutCliente varchar(12),
primary key(numCuenta, rutCliente),
foreign key (numCuenta) references CuentaBancaria(numCuenta),
foreign key (rutCliente) references Cliente(rutCliente));

--numCuentaEmisor: El que realiza la transacción
--numCuentaReceptor: El que recibe la transacción, si corresponde (puede ser null)
create table Transaccion(
codTrans integer,
numCuentaEmisor integer,
numCuentReceptor integer,
fechaTrans date,
montoTrans integer,
descripcionOpcional varchar(50),
codTipoTrans integer,
primary key(codTrans),
foreign key(numCuentaEmisor) references CuentaBancaria(numCuenta),
foreign key(numCuentReceptor) references CuentaBancaria(numCuenta) );

create table Tarjeta(
codTarjeta varchar(12),
codCuentaBancaria integer,
password integer,
primary key(codTarjeta),
foreign key(codCuentaBancaria) references CuentaBancaria(numCuenta));

create table ClaveUnica(
codClaveUnica integer,
numCuentaBancaria integer,
matrizValores varchar(100),
primary key(codClaveUnica),
foreign key (numCuentaBancaria) references CuentaBancaria(numCuenta)
);

create table usuarioCajero(
	rutCajero varchar(12),
	passwordCajero varchar(20),
	primary key (rutCajero)
);
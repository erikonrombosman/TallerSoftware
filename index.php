<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">
  <title>Login Stevan co.</title>
  <!-- Bootstrap core CSS-->
  <link href="view/vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">
  <!-- Custom fonts for this template-->
  <link href="view/vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
  <!-- Custom styles for this template-->
  <link href="view/css/sb-admin.css" rel="stylesheet">
</head>

<body class="bg-dark">
  <div class="center"><label> <font size = 10, color="white"> Bienvenido al Portal Web </font> </label></div>
  <div class="container">
    <div class="card card-login mx-auto mt-5">
      <div class="card-header">Login</div>
      <div class="card-body">
        
        <form method="POST">
          <div class="form-group">
            <label for="exampleInputEmail1">Rut :</label>
            <input class="form-control" id="exampleInputEmail1" type="text" aria-describedby="emailHelp" placeholder="Ingrese Rut Cliente" name="selectRut">
          </div>
          <div class="form-group">
            <label for="exampleInputPassword1">Contraseña:</label>
            <input class="form-control" id="exampleInputPassword1" type="password" placeholder="Contraseña Cliente" name="selectClave">
          </div>
          <div class="form-group">
            <div class="form-check">
              <label class="form-check-label">
                <input class="form-check-input" type="checkbox"> Recordar Contraseña</label>
            </div>
          </div>
          <!--<a class="btn btn-primary btn-block">Ingresar</a> -->
		  <input type="submit" value="Enviar" class="btn btn-primary btn-block">
        </form>

		<?php
		
		//require_once "model/crud.php";
		require_once "controller/controller.php";
		$ingreso = new MvcController();
		$ingreso -> ingresoUsuarioController();
	
		if(isset($_GET["action"])){

		if($_GET["action"] == "fallo"){

				 echo "<br><div class= 'alert alert-danger' role='alert'>  Fallo al ingresar  </div>" ;
	       //echo "fallo al ingresar";
			}

		}

		?>
        <!--<div class="text-center">
          <a class="d-block small mt-3" href="register.html">Register an Account</a>
          <a class="d-block small" href="forgot-password.html">Forgot Password?</a>
        </div> -->
      </div>
    </div>
  </div>
  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
</body>

</html>
